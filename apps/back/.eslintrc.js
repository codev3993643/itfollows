module.exports = {
  root: true,
  extends: ['custom/nest', 'plugin:prettier/recommended'],
  rules: {
    '@typescript-eslint/no-extraneous-class': 'off',
    '@typescript-eslint/consistent-type-imports': 'off',
    'jest/expect-expect': 'off',
  },
};
