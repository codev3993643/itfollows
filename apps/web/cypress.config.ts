// eslint-disable-next-line import/no-extraneous-dependencies -- Cypress is required
import { defineConfig } from 'cypress';

export default defineConfig({
  component: {
    devServer: {
      framework: 'next',
      bundler: 'webpack',
    },
  },
});
